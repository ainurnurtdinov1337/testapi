from rest_framework.routers import DefaultRouter
from .views import ClientViewSet
from django.urls import path, include

app_name = 'api'


api_router = DefaultRouter()
api_router.register('clients', ClientViewSet, basename='clients')



urlpatterns = [
    path('v1/', include(api_router.urls)),
]