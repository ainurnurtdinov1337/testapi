from django.db import models
from phonenumber_field.modelfields import PhoneNumberField, PhoneNumberPrefixWidget

#class CountryCode(models.Model):
#    country_code = PhoneNumberPrefixWidget(blank=False, null=False)

class Client(models.Model):
    import pytz
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    phone = PhoneNumberField(null=False, blank=False, unique=True)
    country_code = models.IntegerField(blank=False, null=False)
    tag = models.CharField(max_length=150, null=False, blank=False)
    time_zone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')


class Message(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=32)
    mailing_id = models.ForeignKey(Mailing, on_delete=models.CASCADE,
        related_name='messages', verbose_name='message')
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE,
        related_name='messages', verbose_name='message')


class Mailing(models.Model):
    start_date = models.DateTimeField(null=False, blank=False)
    text = models.TextField(blank=False, null=False)
    client_filter = models.




